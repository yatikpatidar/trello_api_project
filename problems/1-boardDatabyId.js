const obj = {
    'key': '7bccfa9b4b0c0f95210841692d91818c',
    'token': 'ATTA28b56d395c0bed67cf2b81965c9c10d98bf7861b554d2a1559b5497ac089b87b4CBBFE02'
}

function getBoardData(boardId) {

    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/${boardId}?key=${obj['key']}&token=${obj['token']}`)
            .then(response => {
                return response.json()
            })
            .then(text => {

                resolve(text)
            })
            .catch(err => {

                reject(err.message)
            });
    })

}

module.exports = { getBoardData } 