// Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists. Do note that the cards should be fetched simultaneously from all the lists.

const { getLists } = require("./3-getLists")
const { getCardsById } = require("./4-getCards")
function getAllCardsInBoard(boardId) {

    return new Promise((resolve, reject) => {

        // from here we get array of all lists
        getLists(boardId).then((listArray) => {

            const promiseWait = listArray.map((key) => {
                // from here we get array of card in particular list
                return getCardsById(key['id'])
            })

            Promise.all(promiseWait).then(cardArray => {

                const cardsList = []
                cardArray.forEach((card) => {
                    cardsList.push(card)
                })
                resolve(cardsList)

            }).catch(err => {

                reject(err.message)
            });

        })
    })

}

module.exports = { getAllCardsInBoard }