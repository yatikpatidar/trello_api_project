const obj = {
    'key': '7bccfa9b4b0c0f95210841692d91818c',
    'token': 'ATTA28b56d395c0bed67cf2b81965c9c10d98bf7861b554d2a1559b5497ac089b87b4CBBFE02'
}
function createBoard(name) {

    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/?name=${name}&key=${obj['key']}&token=${obj['token']}`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(text => {

                resolve(text)
            })
            .catch(err => {

                reject(err.message)
            });
    })
}

module.exports = { createBoard }