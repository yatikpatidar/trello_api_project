const { createBoardListCard } = require("./6-createBoardListCard")
const { getLists } = require('./3-getLists')

const obj = {
    'key': '7bccfa9b4b0c0f95210841692d91818c',
    'token': 'ATTA28b56d395c0bed67cf2b81965c9c10d98bf7861b554d2a1559b5497ac089b87b4CBBFE02'
}

function deleteList(id) {

    return new Promise((resolve, reject) => {
        
        fetch(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${obj['key']}&token=${obj['token']}`, {
            method: 'PUT'
        })
            .then(response => {
                console.log(
                    ` Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then((text) => {
                resolve(text)
            })
            .catch(err => reject(err))
    })
}

function deleteSimultaneously() {

    return new Promise((resolve, reject) => {

        createBoardListCard().then((boardData) => {

            const boardId = boardData['id']
            getLists(boardId).then((listData) => {

                const deletePromise = []
                listData.forEach((key) => {

                    const listId = key['id']
                    const status = deleteList(listId)
                    deletePromise.push(status)

                });
                Promise.all(deletePromise).then((data) => {

                    resolve(data)

                }).catch((err) => {

                    reject(err)
                })
            })
        })
    })


}

module.exports = { deleteSimultaneously }