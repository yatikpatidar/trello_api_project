// Create a new board, create 3 lists simultaneously, and a card in each list simultaneously

const { createBoard } = require('./2-createBoard')

const obj = {
    'key': '7bccfa9b4b0c0f95210841692d91818c',
    'token': 'ATTA28b56d395c0bed67cf2b81965c9c10d98bf7861b554d2a1559b5497ac089b87b4CBBFE02'
}

function createList(boardId, listName) {

    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/${boardId}/lists?name=${listName}&key=${obj['key']}&token=${obj['token']}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        }).then((response) => response.json())
            .then((data) => resolve(data))
            .catch((err) => reject(err))
    })
}

function createCard(listId, cardname) {

    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/cards?idList=${listId}&name=${cardname}&key=${obj['key']}&token=${obj['token']}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        }).then((response) => response.json())
            .then((data) => resolve(data))
            .catch((err) => reject(err))
    })
}


function createBoardListCard() {
    const name = "newBoard"
    return new Promise((resolve, reject) => {

        createBoard(name).then((boardData) => {

            const boardId = boardData['id']
            const boardName = boardData['name']

            const promiseList = []
            for (let index = 1; index <= 3; index++) {
                const status = createList(boardId, `${boardName + index + "list"}`)
                promiseList.push(status)
            }
            Promise.all(promiseList).then((data) => {
                const promiseCards = []

                data.forEach((key) => {
                    const listId = key['id']
                    const status = createCard(listId, `${key['name'] + " Sample Cards"}`)
                    promiseCards.push(status)

                })

                Promise.all(promiseCards).then((data) => {
                    resolve(boardData)
                }).catch((err) => reject(err))

            })
        })
    })
}

module.exports = { createBoardListCard }