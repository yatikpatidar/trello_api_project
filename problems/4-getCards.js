// Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data

const obj = {
    'key': '7bccfa9b4b0c0f95210841692d91818c',
    'token': 'ATTA28b56d395c0bed67cf2b81965c9c10d98bf7861b554d2a1559b5497ac089b87b4CBBFE02'
}

function getCardsById(listId) {

    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists/${listId}/cards?key=${obj['key']}&token=${obj['token']}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        }).then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        }).then(text => {

            resolve(text)
        }).catch(err => {

            reject(err.message)
        });
    })

}

module.exports = { getCardsById } 