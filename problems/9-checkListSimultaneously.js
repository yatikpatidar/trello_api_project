const { getLists } = require('./3-getLists')
const { getCardsById } = require('./4-getCards')

const obj = {
    'key': '7bccfa9b4b0c0f95210841692d91818c',
    'token': 'ATTA28b56d395c0bed67cf2b81965c9c10d98bf7861b554d2a1559b5497ac089b87b4CBBFE02'
}

function getCheckListById(id) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/checklists/${id}/?key=${obj['key']}&token=${obj['token']}`, {
            method: 'PUT'
        }).then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        }).then((data) => {
            resolve(data)
        }).catch((err) => {
            reject(err.message)
        })

    })
}

function changeStateOfCheckItems(checkItemId, cardId, state) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${state}&key=${obj['key']}&token=${obj['token']}`, {
            method: 'PUT'
        }).then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        }).then((res) => {
            resolve(res)

        }).catch((err) => {
            reject(err)
        })

    })
}


function checkListSimultaneously(boardId) {
    return new Promise((resolve, reject) => {
        getLists(boardId).then((listData) => {

            // getting ID of first list
            const listId = listData[0]['id']

            getCardsById(listId).then((cardsList) => {

                // getting ID of first card in list
                const cardId = cardsList[0]['id']

                // getting ID of first checklist out of multiple checklist in card
                const checkListId = cardsList[0]['idChecklists'][0]

                // getting all checklist in card's first checklist
                getCheckListById(checkListId).then((data) => {

                    const cardId = data['idCard']
                    const checkPromiseList = []

                    data['checkItems'].forEach(key => {
                        const checkItemId = key['id']
                        const status = changeStateOfCheckItems(checkItemId, cardId, 'complete')
                        checkPromiseList.push(status)
                    });

                    Promise.all(checkPromiseList).then((result) => {
                        resolve(result)
                    }).catch((err) => {
                        reject(err)
                    })
                })
            })
        })
    })
}


module.exports = { checkListSimultaneously }